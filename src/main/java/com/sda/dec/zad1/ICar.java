package com.sda.dec.zad1;

public interface ICar {
    public double getEngineCapacity();
    public int getHorsepower();
    public boolean isHasCharger();
    public double getChargerPressure();
}
