package com.sda.dec.przyklad;

public interface IHero {
    public String getName();
    public int getHp();
    public int getAttackPoints();
    public int getDefencePoints();
}
