package com.sda.pr.przyklad;

public interface ISamochod {
    public void zakluczDrzwi();
    public void wlaczRadio();
    public void uruchomSilnik();
}
