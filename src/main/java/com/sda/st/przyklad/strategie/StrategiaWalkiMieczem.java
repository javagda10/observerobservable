package com.sda.st.przyklad.strategie;

import com.sda.st.przyklad.IStrategia;

public class StrategiaWalkiMieczem implements IStrategia {
    @Override
    public void walcz() {
        System.out.println("Ciach mieczem!");
    }
}
