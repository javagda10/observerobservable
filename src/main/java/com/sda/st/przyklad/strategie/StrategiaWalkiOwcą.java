package com.sda.st.przyklad.strategie;

import com.sda.st.przyklad.IStrategia;

public class StrategiaWalkiOwcą implements IStrategia {
    @Override
    public void walcz() {
        System.out.println("Podkładam smokowi owieczkę.");
    }
}
