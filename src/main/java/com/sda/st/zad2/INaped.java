package com.sda.st.zad2;

public interface INaped {
    int getPoborPraduSilnika();
    int getPoborPaliwa();
    int getMocSilnika();
}
