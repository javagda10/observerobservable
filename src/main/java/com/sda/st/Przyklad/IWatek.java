package com.sda.st.Przyklad;

public interface IWatek {
    public void stop();
    public void start();
    public void pause();

}
