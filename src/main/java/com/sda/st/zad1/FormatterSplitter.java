package com.sda.st.zad1;

public class FormatterSplitter implements IFormatterCzcionki {

    @Override
    public String formatuj(String tekstDoSfromatowania) {
        return tekstDoSfromatowania.replaceAll("", " ").trim();
    }
}
