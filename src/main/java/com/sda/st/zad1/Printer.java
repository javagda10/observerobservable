package com.sda.st.zad1;

public class Printer {
    private IFormatterCzcionki formatterCzcionki;

    public Printer() {
        formatterCzcionki = new FormatterInverter();
    }

    public void setFormatterCzcionki(IFormatterCzcionki formatterCzcionki) {
        this.formatterCzcionki = formatterCzcionki;
    }

    public String formatuj(String tekstDoSformatowania) {
        return formatterCzcionki.formatuj(tekstDoSformatowania);
    }
}
