package com.sda.st.asynchroniczna_pralka;

public class Main {
    public static void main(String[] args) {
        Pralka pralka = new Pralka();

        pralka.setTrybPracy(new SzybkiePranie());

        pralka.startPralka();
    }
}
