package com.sda.ad.przyklad;

public interface ISwichable {
    public void switch_on();

    public void switch_off();
}
