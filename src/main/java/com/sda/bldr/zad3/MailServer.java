package com.sda.bldr.zad3;

import java.util.ArrayList;
import java.util.List;

public class MailServer {
    private List<Client> clients = new ArrayList<>();

    public void connect(Client c) {
        clients.add(c);
    }

    public void disconnect(Client c) {
        clients.remove(c);
    }

    public void sendMessage(Mail m, Client sender) {
        for (Client c : clients) {
            if(c != sender){
                c.readMail(m);
            }
        }
    }
}
