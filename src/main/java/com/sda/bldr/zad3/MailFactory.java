package com.sda.bldr.zad3;

import java.time.LocalDateTime;

public abstract class MailFactory {
    public static Mail stworzMailaOfertowego(String tresc) {
        return new Mail.Builder()
                .setType(MailType.OFFER)
                .setTresc(tresc)
                .setSzyfrowane(false)
                .setNadawca("Allegro")
                .setDataNadania(LocalDateTime.now())
                .setIpWysylajacy("10.0.1.50")
                .setNazwaSkrzynki("Oferty")
                .createMail();
    }
}
