package com.sda.bldr.zad3;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private String imie;
    private List<Mail> skrzynkaOdbiorcza = new ArrayList<>();

    public Client(String imie) {
        this.imie = imie;
    }

    public void readMail(Mail m){
        skrzynkaOdbiorcza.add(m);
        System.out.println("Klient " + this.getImie() + " otrzymal maila");
    }

    public String getImie() {
        return imie;
    }

    public List<Mail> getSkrzynkaOdbiorcza() {
        return skrzynkaOdbiorcza;
    }
}
