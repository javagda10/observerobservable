package com.sda.bldr.zad3;

public class Main {
    public static void main(String[] args) {
        Mail.Builder builder = new Mail.Builder();

        MailServer server = new MailServer();

        Client darek = new Client("Darek");

        server.connect(new Client("Marian"));
        server.connect(new Client("Marek"));
        server.connect(new Client("Jarek"));
        server.connect(darek);

        builder.setNadawca("Marian")
                .setProtokolKomunikacji("ipv4")
                .setSzyfrowane(false)
                .setTresc("Siema!")
                .setType(MailType.NOTIFICATIONS);

        server.sendMessage(builder.createMail(), darek);
    }
}
