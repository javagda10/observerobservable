package com.sda.bldr.zad3;

import java.time.LocalDateTime;

public class Mail {
    private String tresc, nadawca;
    private LocalDateTime dataNadania;
    private LocalDateTime dataOdbioru;
    private String ipWysylajacy, ipOdbiorca;

    private String serverPosredni, nazwaSkrzynki, protokolKomunikacji;

    private MailType type;
    private boolean szyfrowane, spam;

    public Mail(String tresc, String nadawca, LocalDateTime dataNadania, LocalDateTime dataOdbioru, String ipWysylajacy, String ipOdbiorca, String serverPosredni, String nazwaSkrzynki, String protokolKomunikacji, MailType type, boolean szyfrowane, boolean spam) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.dataNadania = dataNadania;
        this.dataOdbioru = dataOdbioru;
        this.ipWysylajacy = ipWysylajacy;
        this.ipOdbiorca = ipOdbiorca;
        this.serverPosredni = serverPosredni;
        this.nazwaSkrzynki = nazwaSkrzynki;
        this.protokolKomunikacji = protokolKomunikacji;
        this.type = type;
        this.szyfrowane = szyfrowane;
        this.spam = spam;
    }
    public static class Builder{

        private String tresc;
        private String nadawca;
        private LocalDateTime dataNadania;
        private LocalDateTime dataOdbioru;
        private String ipWysylajacy;
        private String ipOdbiorca;
        private String serverPosredni;
        private String nazwaSkrzynki;
        private String protokolKomunikacji;
        private MailType type;
        private boolean szyfrowane;
        private boolean spam;

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setDataNadania(LocalDateTime dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setDataOdbioru(LocalDateTime dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Builder setIpWysylajacy(String ipWysylajacy) {
            this.ipWysylajacy = ipWysylajacy;
            return this;
        }

        public Builder setIpOdbiorca(String ipOdbiorca) {
            this.ipOdbiorca = ipOdbiorca;
            return this;
        }

        public Builder setServerPosredni(String serverPosredni) {
            this.serverPosredni = serverPosredni;
            return this;
        }

        public Builder setNazwaSkrzynki(String nazwaSkrzynki) {
            this.nazwaSkrzynki = nazwaSkrzynki;
            return this;
        }

        public Builder setProtokolKomunikacji(String protokolKomunikacji) {
            this.protokolKomunikacji = protokolKomunikacji;
            return this;
        }

        public Builder setType(MailType type) {
            this.type = type;
            return this;
        }

        public Builder setSzyfrowane(boolean szyfrowane) {
            this.szyfrowane = szyfrowane;
            return this;
        }

        public Builder setSpam(boolean spam) {
            this.spam = spam;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, dataNadania, dataOdbioru, ipWysylajacy, ipOdbiorca, serverPosredni, nazwaSkrzynki, protokolKomunikacji, type, szyfrowane, spam);
        }
    }
}
