package com.sda.bldr.zad3;

public enum MailType {
    UNKNOWN, OFFER, SOCIAL, NOTIFICATIONS, FORUM;
}
