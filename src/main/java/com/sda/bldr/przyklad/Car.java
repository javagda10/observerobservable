package com.sda.bldr.przyklad;

public class Car {
    private int capacity;
    private String name;

    public Car(int capacity, String name) {
        this.capacity = capacity;
        this.name = name;
    }
    public static class Builder {

        private int capacity;
        private String name;

        public Builder setCapacity(int capacity) {
            this.capacity = capacity;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Car createCar() {
            return new Car(capacity, name);
        }
    }
}
