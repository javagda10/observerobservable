package com.sda.bldr.zad2;

public class Main {
    public static void main(String[] args) {
        Stamp.Builder builder = new Stamp.Builder();

        builder.setSecondDayNumber(9);
        builder.setCaseNumber(10);

        System.out.println(builder.createStamp());

        builder.setCaseNumber(11);
        System.out.println(builder.createStamp());
    }
}
