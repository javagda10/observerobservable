package com.sda.oo.zad1_threaded;

import javafx.beans.property.ObjectProperty;

public class MessageRequest implements Runnable {
    private Message wiadomosc;
    private ISender stacja;

    public MessageRequest(Message wiadomosc, ISender sender) {
        this.wiadomosc = wiadomosc;
        this.stacja = sender;
    }

    @Override
    public void run() {
//        System.out.println("Rozpoczynam nadawanie wiadomosci ");
        try {
            Thread.sleep(wiadomosc.getTresc().length() * 100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stacja.rozeslij(wiadomosc);
//        System.out.println("Wiadomosc rozeslana: ");
    }
}
