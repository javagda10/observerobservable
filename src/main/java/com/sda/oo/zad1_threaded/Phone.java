package com.sda.oo.zad1_threaded;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.ArrayList;
import java.util.List;

public class Phone implements ChangeListener<Message> {
    private String phoneNumber;
    private List<String> inbox = new ArrayList<>();

    public Phone(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "phoneNumber='" + phoneNumber + '\'' +
                ", inbox=" + inbox +
                '}';
    }

    @Override
    public void changed(ObservableValue<? extends Message> observable, Message oldValue, Message wiadomosc) {
        if (wiadomosc.getNumer().equals(phoneNumber)) {
            inbox.add(wiadomosc.getTresc());
            System.out.println(phoneNumber + " otrzymalem wiadomosc: " + wiadomosc.getTresc());
//            System.out.println("To tresc do mnie " + this);
        } else {
//            System.out.println("To nie do mnie : " + phoneNumber);
        }
    }
}
