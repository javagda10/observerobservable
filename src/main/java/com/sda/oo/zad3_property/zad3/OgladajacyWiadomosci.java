package com.sda.oo.zad3_property.zad3;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.util.Observable;
import java.util.Observer;

public class OgladajacyWiadomosci implements ChangeListener<Wiadomosc> {
    private String imie;
    private int progZainteresowania;

    public OgladajacyWiadomosci(String imie, int progZainteresowania) {
        this.imie = imie;
        this.progZainteresowania = progZainteresowania;
    }

//    @Override
//    public void update(Observable o, Object arg) {
//        if (arg instanceof Wiadomosc) {
//            Wiadomosc wiadomosc = (Wiadomosc) arg;
//            if (wiadomosc.getWaga() >= progZainteresowania) {
//                System.out.println("Panika! " + imie + " panikuje na wiadomość o:" + wiadomosc.getTresc());
//            } else {
//                System.out.println("A mnie (" + imie + ") to nie robi...");
//            }
//        }
//    }

//    @Override
//    public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
//        System.out.println("Zostaje (" + imie + ") powiadomiony o nowej wiadomosci: " + newValue);
//    }

    @Override
    public void changed(ObservableValue<? extends Wiadomosc> observable, Wiadomosc oldValue, Wiadomosc wiadomosc) {
        if (wiadomosc.getWaga() >= progZainteresowania) {
            System.out.println("Panika! " + imie + " panikuje na wiadomość o:" + wiadomosc.getTresc());
        } else {
            System.out.println("A mnie (" + imie + ") to nie robi...");
        }
    }
}
