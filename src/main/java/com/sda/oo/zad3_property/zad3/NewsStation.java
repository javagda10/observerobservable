package com.sda.oo.zad3_property.zad3;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Observable;

public class NewsStation {

//    private StringProperty wiadomosc = new SimpleStringProperty();
    private ObjectProperty<Wiadomosc> wiadomosc = new SimpleObjectProperty<>();

    public void addObserver(OgladajacyWiadomosci ogladajacyWiadomosci) {
        wiadomosc.addListener(ogladajacyWiadomosci);
    }

    public void powiadomOWiadomosci(int waga, String tresc) {
        Wiadomosc news = new Wiadomosc(waga, tresc);

        wiadomosc.setValue(news);
//        setChanged();
//        notifyObservers(wiadomosc);
    }
}
