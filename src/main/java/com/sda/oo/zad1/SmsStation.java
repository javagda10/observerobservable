package com.sda.oo.zad1;

import java.util.Observable;

public class SmsStation extends Observable{

    public void addPhone(Phone p) {
        addObserver(p);
    }

    public void removePhone(Phone p) {
        deleteObserver(p);
    }

    public void sendSms(String numerDoKogo, String tresc) {
        Message wiadomosc = new Message(numerDoKogo, tresc);

        setChanged();
        notifyObservers();
    }

    public void sendMms(String numerDoKogo, String tresc, boolean costam) {
        MMSMessage wiadomosc = new MMSMessage(numerDoKogo, tresc, costam);

        setChanged();
        notifyObservers(wiadomosc);
    }
}
