package com.sda.oo.zad1_property.zad1;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.Observable;

public class SmsStation {

    private ObjectProperty<Message> lastMessage = new SimpleObjectProperty<>();

    public void addPhone(Phone p) {
        lastMessage.addListener(p);
    }

    public void removePhone(Phone p) {
        lastMessage.removeListener(p);
    }

    public void sendSms(String numerDoKogo, String tresc) {
        Message wiadomosc = new Message(numerDoKogo, tresc);

        lastMessage.setValue(wiadomosc);
    }
}
