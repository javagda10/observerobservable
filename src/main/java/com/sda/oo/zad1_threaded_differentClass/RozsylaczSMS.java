package com.sda.oo.zad1_threaded_differentClass;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class RozsylaczSMS implements ISender {
    private ObjectProperty<Message> lastMessage = new SimpleObjectProperty<>();

    public void addPhone(Phone p) {
        lastMessage.addListener(p);
    }

    public void removePhone(Phone p) {
        lastMessage.removeListener(p);
    }


    public void rozeslij(Message wiadomosc){
        lastMessage.setValue(wiadomosc);
    }
}
