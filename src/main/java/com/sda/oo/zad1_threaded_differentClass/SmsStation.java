package com.sda.oo.zad1_threaded_differentClass;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmsStation {

    private ExecutorService pula = Executors.newFixedThreadPool(3);
    private RozsylaczSMS rozsylaczSMS = new RozsylaczSMS();

    public void addPhone(Phone p) {
        rozsylaczSMS.addPhone(p);
    }

    public void removePhone(Phone p) {
        rozsylaczSMS.removePhone(p);
    }

    public void sendSms(String numerDoKogo, String tresc) {
        Message wiadomosc = new Message(numerDoKogo, tresc);

        pula.submit(new MessageRequest(wiadomosc, rozsylaczSMS));
//        lastMessage.setValue(wiadomosc);
    }

}
