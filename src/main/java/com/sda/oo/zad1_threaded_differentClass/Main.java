package com.sda.oo.zad1_threaded_differentClass;

public class Main {
    public static void main(String[] args) {
        SmsStation station = new SmsStation();

        Phone telefon1 = new Phone("666");
        Phone telefon2 = new Phone("999");
        Phone telefon3 = new Phone("112");

        station.addPhone(telefon1);
        station.addPhone(telefon2);
        station.addPhone(telefon3);

        station.sendSms("666", "Trescaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        station.sendSms("666", "Tresdddddddddddddddddddddddddddddddddddddddddddddddddddddddc");
        station.sendSms("666", "TrescTrescTrescTrescTrescTrescTrescTrescTrescTrescTrescTresc");
        station.sendSms("112", "Trescescescescescescesc");
        station.sendSms("999", "Treaaaaaaaaaaaaaaaaaaaaaaaaasc");
        station.sendSms("666", "Tresdddddddddddddddddddddc");
        station.sendSms("112", "Trescwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
    }
}
