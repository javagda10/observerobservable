package com.sda.af.car;

import java.awt.*;

class Car implements ICar {
    private String brand;
    private String model;
    private double engineCapacity;
    private int horsepower;
    private String licensePlateNumber;
    private Color color;


    public Car(String brand, String model, double engineCapacity, int horsepower, String licensePlateNumber, Color color) {
        this.brand = brand;
        this.model = model;
        this.engineCapacity = engineCapacity;
        this.horsepower = horsepower;
        this.licensePlateNumber = licensePlateNumber;
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public void setLicensePlateNumber(String licensePlateNumber) {
        this.licensePlateNumber = licensePlateNumber;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
