package com.sda.af.computers;

public class AsusPC extends AbstractPC {
    public AsusPC(String name, int cpuPower, double gpuPower, boolean isOverclocked) {
        super(name, COMPUTER_BRAND.ASUS, cpuPower, gpuPower, isOverclocked);
    }

    public static AbstractPC createN53() {
        return new AsusPC("N53", 50, 50.0, false);
    }

    public static AbstractPC createV2() {
        return new AsusPC("V2", 70, 40.0, true);
    }
}
