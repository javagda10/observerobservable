package com.sda.af.computers;

public class AbstractPC {
    protected String name;
    protected COMPUTER_BRAND brand;
    protected int cpuPower;
    protected double gpuPower;
    protected boolean isOverclocked;

    public AbstractPC(String name, COMPUTER_BRAND brand, int cpuPower, double gpuPower, boolean isOverclocked) {
        this.name = name;
        this.brand = brand;
        this.cpuPower = cpuPower;
        this.gpuPower = gpuPower;
        this.isOverclocked = isOverclocked;
    }

}
